/**
 * Created by Wong Shin Yueh on 21 Oct 2016
 * FSF3 - Client-side foundation assessment
 * Purpose: to write the application controller
 */
(function(){
    var RegApp = angular.module("RegApp", []);

    var MyCtrl = function ($http) {
        var myCtrl = this;

        var defaultuserObject = {
            email: "",
            password:"",
            name: "",
            gender: "",
            dob: "",
            address: "",
            country: "",
            phone: "",
        }

        myCtrl.userObject = angular.copy(defaultuserObject);
        myCtrl.users = [];

        myCtrl.isAgeValid = function(){
            var date = new Date(myCtrl.userObject.dob);
            date.setFullYear(date.getFullYear() + 18);

            return (date >= new Date());
        }

        myCtrl.server_response = "";

        myCtrl.submit = function submit(){
            console.log(myCtrl.userObject);
            myCtrl.users.push(myCtrl.userObject);

            $http
                .post("/api/users",myCtrl.userObject)
                .then(function (response) {
                    myCtrl.server_response = "Thank You! You have successfully registered.";
                    console.log(myCtrl.server_response);
                })
                .catch(function (response) {
                    myCtrl.server_response = "Sorry, your registration is unsuccessful. Please try again later";
                    console.log(myCtrl.server_response);
                });

            myCtrl.userObject = angular.copy(defaultuserObject);
        }

        console.log("The controller has started...");
    };

    RegApp.controller("MyCtrl", [ "$http", MyCtrl ]);
})();