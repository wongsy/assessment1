/*
 * Created by Wong Shin Yueh on 21 Oct 2016
 * FSF3 - Client-side foundation assessment
 * Purpose: Server-side code to handle client requests
 **/

const express = require("express");
const app = express();

const path = require("path");
const docroot = path.join(__dirname, "..");

app.set("port", process.argv[2] || process.env.NODE_PORT || 3000);
app.use(express.static(path.join(docroot, "client")));
app.use("/bower_components",
    express.static(path.join(docroot, "bower_components")));

app.listen(app.get("port"), function () {
    console.log("Server running at http://localhost: %s", app.get("port"));
});

// parse body of req
var bodyParser = require('body-parser');
app.use(bodyParser.json());                     // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application


app.post("/api/users", function (req, res) {
    console.log(req.body);
    res.json(req.body);
});


//404 Handler - Page not found
app.use(function(req, res) {
    res.redirect('404.html');
});